## Git/GitLab Summary

Software developement requires highly accurate codes which are well oriented and managed, **Git** is that wprkspace which allows you to do so at an individual as well as team collabrative level.

It is a distributive version control system, which enables the programmer to:

   * Store Content
   * Parallel adding of code
   * Track of changes managed

### Terms to be known:

Repository : collection of file and folders(codes).

GitLab : Second most efficient Git Repository apart from GitHub.

Commit : Save option, that'll apply the changes you made to any file.The changes will be made only to your local machine repository.

Push : This syncs your files and the changes made within to the Gitlab.

Branch : Within your main master file you can further add other files too, and those refered to as the branches.

Merge : Integrating/adding two branches as an when ready or bug free(according you)\.

Clone : Copies the file to your local machine repository.

### Installing Git
For linux : in terminal type

   > sudo apt-get install git

*(Git works only on Ubuntu)*

For Windows : 
   [Download the installer](https://git-scm.com/download/win)

Following are the internals of Git
![Internals](https://www.google.com/search?q=git+internal&rlz=1C1CHBD_enIN859IN859&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjYk46BpOvqAhUVT30KHU9vB_UQ_AUoAXoECA0QAw&biw=1366&bih=657#imgrc=yJ5auW_Zw5U_aM)



